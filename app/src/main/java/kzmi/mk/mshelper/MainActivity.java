package kzmi.mk.mshelper;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Character> playerList = new ArrayList<Character>();
    SharedPreferences sharedPref;
    private Calendar c;
    private TextView timer;
    private ImageView charImg;
    private ImageView legionImg;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(this, AlarmReceiver.class);
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        if(PendingIntent.getBroadcast(MainActivity.this, 0, alarmIntent, PendingIntent.FLAG_NO_CREATE) == null) {
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, 3);
            calendar.set(Calendar.MINUTE, 21);
            calendar.set(Calendar.SECOND, 0);
            if (calendar.before(Calendar.getInstance())) {
                calendar.add(Calendar.DATE, 1);
            }
            PendingIntent pi = PendingIntent.getBroadcast(this, 99, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 86400000, pi);
        }

        if(sharedPref.contains("charList")) {
            loadList();
        }
        else {
            playerList.add(new Character("CORSChar", "Corsair", 250));
            playerList.add(new Character("HEROChar", "Hero", 250));
        }

        Button charButton = findViewById(R.id.toCharButton);
        Button legionButton = findViewById(R.id.toLegionButton);
        charImg = findViewById(R.id.mainCharImg);
        legionImg = findViewById(R.id.mainLegionImg);
        timer = findViewById(R.id.timeTillReset);

        AssetManager assetManager = getAssets();
        try {
            InputStream ims = assetManager.open("crop1.png");
            Drawable d = Drawable.createFromStream(ims, null);
            charImg.setImageDrawable(d);
            ims = assetManager.open("crop2.png");
            d = Drawable.createFromStream(ims, null);
            legionImg.setImageDrawable(d);
        } catch (IOException e) {
            return;
        }

        c = Calendar.getInstance();
        c.setTimeInMillis(86400000-c.getTimeInMillis());
        final SimpleDateFormat DATE_TIME = new SimpleDateFormat("HH:mm:ss");
        DATE_TIME.setTimeZone(TimeZone.getTimeZone("UTC"));
        timer.setText(DATE_TIME.format(c.getTimeInMillis()));

        charButton.setBackgroundColor(Color.TRANSPARENT);
        legionButton.setBackgroundColor(Color.TRANSPARENT);
        charButton.setTextColor(Color.TRANSPARENT);
        legionButton.setTextColor(Color.TRANSPARENT);

        charButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),CharacterHubActivity.class);
                intent.putExtra("CHARLIST", playerList);
                startActivityForResult(intent,1);
            }
        });


        legionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LegionHubActivity.class);
                intent.putExtra("CHARLIST", playerList);
                startActivityForResult(intent, 2);
            }
        });

        Thread thread = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                c = Calendar.getInstance();
                                c.setTimeInMillis(86400000-c.getTimeInMillis());
                                timer.setText(DATE_TIME.format(c.getTimeInMillis()));
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        thread.start();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                playerList = (ArrayList<Character>) data.getExtras().get("CHARLIST");
            }
        }
        if (requestCode == 99) {
            if (resultCode == RESULT_OK) {
                dailyWeeklyReset();
            }
        }
    }

    public void dailyWeeklyReset() {
        ArrayList<Character> temp = new ArrayList<Character>();
        Calendar tempCal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        for(Character x : playerList) {
            x.resetDailyBosses();
            temp.add(x);
            if(tempCal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) {
                x.resetWeeklyBosses();
            }
        }
        playerList = temp;
    }


    /*
     * Write the current list of characters and their stored routines to local storage.
     */

    @Override
    public void onPause() {
        super.onPause();
        saveList();
    }

    /*
     * Called only whenever the app is launched. Retrieves the saved character list
     * from local storage.
     */

    public void saveList() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Gson gson = new Gson();
        String json = gson.toJson(playerList);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString("charList", json);
        prefsEditor.commit();
    }

    public void loadList() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Gson gson = new Gson();
        String json = prefs.getString("charList", "");
        Type type = new TypeToken<List<Character>>(){}.getType();
        playerList = gson.fromJson(json, type);
    }


}
