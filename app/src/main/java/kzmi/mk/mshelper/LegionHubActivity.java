package kzmi.mk.mshelper;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class LegionHubActivity extends AppCompatActivity {

    ArrayList<Character> charList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legion_hub);

        charList = (ArrayList<Character>) getIntent().getExtras().get("CHARLIST");

        Button legionStatsButton = findViewById(R.id.legionStatsButton);
        Button legionBoardButton = findViewById(R.id.legionBoardButton);
        legionStatsButton.setTextColor(Color.TRANSPARENT);
        legionStatsButton.setBackgroundColor(Color.TRANSPARENT);
        legionBoardButton.setTextColor(Color.TRANSPARENT);
        legionBoardButton.setBackgroundColor(Color.TRANSPARENT);

        legionStatsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), LegionStatsActivity.class);
                intent.putExtra( "CHARLIST",charList);
                startActivityForResult(intent,1);
            }
        });
    }
}
