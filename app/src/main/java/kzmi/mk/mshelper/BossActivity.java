package kzmi.mk.mshelper;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

/*
    The BossActivity class sets up a custom RecyclerView to view the character's boss routines
    when the user taps on either "Daily Bosses" or "Weekly Bosses" from the RoutinesActivity class
 */
public class BossActivity extends AppCompatActivity {

    ArrayList<Boss> bossList;
    BossViewAdapter bossView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boss);

        if((ArrayList<Boss>) getIntent().getExtras().get("DAILYBOSS") != null) {
            bossList = (ArrayList<Boss>) getIntent().getExtras().get("DAILYBOSS");
        }
        else if ((ArrayList<Boss>) getIntent().getExtras().get("WEEKLYBOSS") != null) {
            bossList = (ArrayList<Boss>) getIntent().getExtras().get("WEEKLYBOSS");
        }

        RecyclerView recyclerView = findViewById(R.id.bossView);
        bossView = new BossViewAdapter(this, bossList);
        recyclerView.setAdapter(bossView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    //Return to RoutineActivity with the updated boss list
    @Override
    public void onBackPressed(){
        bossList = bossView.getBossList();

        Intent intent = new Intent();
        intent.putExtra("NEWBOSS", bossList);
        setResult(RESULT_OK, intent);
        finish();
    }

}