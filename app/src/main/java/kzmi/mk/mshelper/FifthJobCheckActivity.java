package kzmi.mk.mshelper;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class FifthJobCheckActivity extends AppCompatActivity {

    Character currentChar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fifth_job_check);

        currentChar = (Character) getIntent().getExtras().get("FIFTHJOB");

        Button fifthJobButton = findViewById(R.id.fifthJobButton);
        fifthJobButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentChar.setFifthJobCheck(true);
                Intent intent = new Intent();
                intent.putExtra("ARCANERIVERF", currentChar);
                setResult(RESULT_OK, intent);
                finish();
            }

        });
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent();
        intent.putExtra("ARCANERIVERF", currentChar);
        setResult(RESULT_OK, intent);
        finish();
    }
}
