package kzmi.mk.mshelper;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class LegionStatsSubAdapter extends RecyclerView.Adapter<LegionStatsSubAdapter.ViewHolder> {
    ArrayList<Character> charList;
    Context mContext;
    String[] jobArray;
    String[] statArray;
    String[][] valueArray;

    public LegionStatsSubAdapter(Context context, ArrayList<Character> characters) {
        charList = characters;
        this.mContext = context;
        jobArray = mContext.getResources().getStringArray(R.array.jobList_array);
        statArray =  mContext.getResources().getStringArray(R.array.legionStat_array);
        valueArray = new String[][]{mContext.getResources().getStringArray(R.array.tierOne_stats), mContext.getResources().getStringArray(R.array.tierTwo_stats), mContext.getResources().getStringArray(R.array.tierThree_stats), mContext.getResources().getStringArray(R.array.tierFour_stats), mContext.getResources().getStringArray(R.array.tierFive_stats)};
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.legionstats_viewsub, parent, false) ;
        LegionStatsSubAdapter.ViewHolder holder = new LegionStatsSubAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        int temp = 0;
        if(charList.get(i).getLevel() == 0) {
            for(int j = 0; j < jobArray.length; j++) {
                if(jobArray[j].equals(charList.get(i).getJob())) {
                    temp = j;
                    break;
                }
            }
            String s = statArray[temp] + valueArray[0][temp];
            viewHolder.stats.setText(s);
            viewHolder.level.setText("");
            viewHolder.job.setText(charList.get(i).getJob());
        }
        else if (charList.get(i).getLegionTier() == 0) {}
        else {
            for(int j = 0; j < jobArray.length; j++) {
                if(jobArray[j].equals(charList.get(i).getJob())) {
                    temp = j;
                    break;
                }
            }
            String s = statArray[temp] + valueArray[charList.get(i).getLegionTier()-1][temp].toString();
            viewHolder.stats.setText(s);
            viewHolder.level.setText(Integer.toString(charList.get(i).getLevel()));
            viewHolder.job.setText(charList.get(i).getJob());
        }
    }

    @Override
    public int getItemCount() {
        return charList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView stats, level, job;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            stats = itemView.findViewById(R.id.legionStats_stat);
            level = itemView.findViewById(R.id.legionStats_level);
            job = itemView.findViewById(R.id.legionStats_job);
        }
    }
}
