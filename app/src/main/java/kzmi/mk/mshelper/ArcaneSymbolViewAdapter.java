package kzmi.mk.mshelper;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class ArcaneSymbolViewAdapter extends RecyclerView.Adapter<ArcaneSymbolViewAdapter.ViewHolder> {
    private ArcaneSymbol[] mArcaneSymbols;
    private Context mContext;

    public ArcaneSymbolViewAdapter(Context context, ArcaneSymbol[] arcaneSymbols) {
        mArcaneSymbols = arcaneSymbols;
        mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.arcanesymbol_view, parent, false) ;
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        if(i == 0) {
            viewHolder.name.setText("Arcane Journey");
            viewHolder.name.setTextColor(Color.parseColor("#0066FF"));
            viewHolder.level.setTextColor(Color.parseColor("#0066FF"));
            viewHolder.daily.setText("Daily(8)");
            viewHolder.daily.setTextColor(Color.parseColor("#0066FF"));
        }
        if(i == 1) {
            viewHolder.name.setText("Chu Chu Island");
            viewHolder.name.setTextColor(Color.parseColor("#00DD00"));
            viewHolder.level.setTextColor(Color.parseColor("#00DD00"));
            viewHolder.daily.setText("Daily(15)");
            viewHolder.daily.setTextColor(Color.parseColor("#00DD00"));
        }
        if(i == 2) {
            viewHolder.name.setText("Lachelein");
            viewHolder.name.setTextColor(Color.parseColor("#BB00FF"));
            viewHolder.level.setTextColor(Color.parseColor("#BB00FF"));
            viewHolder.daily.setText("Daily(5)");
            viewHolder.daily.setTextColor(Color.parseColor("#BB00FF"));
        }
        if(i == 3) {
            viewHolder.name.setText("Arcana");
            viewHolder.name.setTextColor(Color.parseColor("#336688"));
            viewHolder.level.setTextColor(Color.parseColor("#336688"));
            viewHolder.daily.setText("Daily(10)");
            viewHolder.daily.setTextColor(Color.parseColor("#336688"));
        }
        if(i == 4) {
            viewHolder.name.setText("Morass");
            viewHolder.name.setTextColor(Color.parseColor("#FF4444"));
            viewHolder.level.setTextColor(Color.parseColor("#FF4444"));
            viewHolder.daily.setText("Daily(8)");
            viewHolder.daily.setTextColor(Color.parseColor("#FF4444"));
        }
        if(i == 5) {
            viewHolder.name.setText("Esfera");
            viewHolder.name.setTextColor(Color.parseColor("#33AAEE"));
            viewHolder.level.setTextColor(Color.parseColor("#33AAEE"));
            viewHolder.daily.setText("Daily(8)");
            viewHolder.daily.setTextColor(Color.parseColor("#33AAEE"));
        }
        viewHolder.exp.setText(mArcaneSymbols[i].getExp() + "/" + mArcaneSymbols[i].getMaxExp());
        viewHolder.level.setText(mArcaneSymbols[i].getLevel());

        viewHolder.daily.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (i == 0)
                    mArcaneSymbols[i].addExp(8);
                if (i == 1)
                    mArcaneSymbols[i].addExp(15);
                if (i == 2)
                    mArcaneSymbols[i].addExp(5);
                if (i == 3)
                    mArcaneSymbols[i].addExp(10);
                if (i == 4)
                    mArcaneSymbols[i].addExp(8);
                if (i == 5)
                    mArcaneSymbols[i].addExp(8);
                viewHolder.exp.setText(mArcaneSymbols[i].getExp() + "/" + mArcaneSymbols[i].getMaxExp());
                viewHolder.level.setText(mArcaneSymbols[i].getLevel());
            }
        });
        viewHolder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mArcaneSymbols[i].expUp();
                viewHolder.exp.setText(mArcaneSymbols[i].getExp() + "/" + mArcaneSymbols[i].getMaxExp());
                viewHolder.level.setText(mArcaneSymbols[i].getLevel());
            }
        });
        viewHolder.levelUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mArcaneSymbols[i].setLevel(mArcaneSymbols[i].getLevelInt()+1);
                viewHolder.exp.setText(mArcaneSymbols[i].getExp() + "/" + mArcaneSymbols[i].getMaxExp());
                viewHolder.level.setText(mArcaneSymbols[i].getLevel());
            }

        });
    }

    @Override
    public int getItemCount() {
        return 6;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, level, exp;
        Button daily, plus, levelUp;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.symbolName);
            level = itemView.findViewById(R.id.symbolLevel);
            exp = itemView.findViewById(R.id.symbolExp);
            daily = itemView.findViewById(R.id.symbolDailyButton);
            plus = itemView.findViewById(R.id.symbolPlusButton);
            levelUp = itemView.findViewById(R.id.symbolLvlUpButton);
        }
    }
}
