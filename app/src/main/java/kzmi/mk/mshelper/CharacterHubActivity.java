/*
    MSHelper written by Kazami

    Current Version : 0.31
    Last Updated : January 28th, 2019
 */

package kzmi.mk.mshelper;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import java.util.ArrayList;

/*
    CharacterHubActivity is the hub for all other functions for this application. CharacterViewAdapter is
    launched alongside CharacterHubActivity when the app first starts.
 */

public class CharacterHubActivity extends AppCompatActivity {

    ArrayList<Character> playerList = new ArrayList<Character>(); //Stores user created Character objects.
    CharacterViewAdapter characterAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charhub);

        playerList = (ArrayList<Character>) getIntent().getExtras().get("CHARLIST");

        RecyclerView recyclerView = findViewById(R.id.homeView);
        characterAdapter = new CharacterViewAdapter(this, playerList);
        recyclerView.setAdapter(characterAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        FloatingActionButton addCharButton = findViewById(R.id.addCharButton);
        addCharButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(getApplicationContext(), CharacterEditActivity.class);
                startIntent.putExtra("CHAREDIT",new Character ("","",1));
                startIntent.putExtra("ARRAYPOSITION",-1);
                startActivityForResult(startIntent, 3);
            }
        });
    }

    /*
        The following requestCodes correspond to the following application actions
        1 - Returned from the RoutinesActivity class
        2 - Returned from CharacterEditActivity class with a edited existing character
        3 - Returned from CharacterEditActivity class with a new character
     */

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Character updatedChar = (Character) data.getExtras().get("UPDATEDCHAR");
                for (int i = 0; i < playerList.size(); i++) {
                    if (playerList.get(i).getName().equals(updatedChar.getName()))
                        playerList.set(i, updatedChar);
                }
            }
        }
        if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                Character updatedChar = (Character) data.getExtras().get("EDITEDCHAR");
                int i = (Integer) data.getExtras().get("ARRAYPOSITION");
                playerList.set(i,updatedChar);
                characterAdapter.notifyDataSetChanged();
            }
        }
        if (requestCode == 3) {
            if (resultCode == RESULT_OK) {
                Character updatedChar = (Character) data.getExtras().get("EDITEDCHAR");
                playerList.add(updatedChar);
                characterAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent();
        intent.putExtra("CHARLIST", playerList);
        setResult(RESULT_OK, intent);
        finish();
    }

}
