package kzmi.mk.mshelper;

import java.io.Serializable;

/*
    The Boss class contains the  data of an in game enemy boss. Currently this class
    holds a bosses name, how many times the player has cleared the boss, and the max
    amount of clears a player can clear the boss before a reset.
 */
public class Boss implements Serializable {
    private String name;
    private int maxCount;
    private int count;

    public Boss(String name, int maxCount, int count) {
        this.name = name;
        this.maxCount = maxCount;
        this.count = count;
    }

    //Sets the name of the boss to a new name
    public String getName() {
        return name;
    }

    //Sets the name of the boss to a new name
    public void setName(String name) {
        this.name = name;
    }

    //Returns the maximum clear count.
    public int getMaxCount() {
        return maxCount;
    }

    //Sets the maximum clear count to a new value.
    public void setMaxCount(int maxCount) {
        this.maxCount = maxCount;
    }

    //Returns the clear count.
    public int getCount() {
        return count;
    }

    //Sets the clear count to a new value.
    public void setCount(int count) {
        this.count = count;
    }

    //For use only in the BossViewAdapter, increments the clear count by one.
    public void addCount()  {
        this.count++;
    }

    //For use only in the BossViewAdapter, returns the clear and max count as a single string.
    public String getMergedString() {
        return Integer.toString(this.count) + "/" + Integer.toString(this.maxCount);
    }
}
