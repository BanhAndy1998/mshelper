package kzmi.mk.mshelper;

import android.content.res.Resources;

import java.io.Serializable;

public class ArcaneSymbol implements Serializable {
    private int level;
    private int exp;
    private int maxExp;
    private int[] expRef = {12,15,20,27,36,47,60,75,92,111,132,155,180,207,236,267,300,335,372};
    private String color;

    public ArcaneSymbol() {
        this.level = 1;
        this.exp = 0;
        this.maxExp = 12;
    }

    public void expUp() {
        if(level != 20)
            this.exp++;
    }


    public void addExp(int exp) {
        this.exp += exp;
        if(this.exp >= maxExp) {
            int temp = this.exp - this.maxExp;
            setLevel(this.level+1);
            if(level != 20) {
                setExp(temp);
            }
        }
    }

    public String getLevel() {
        return Integer.toString(level);
    }

    public int getLevelInt() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
        if(level >= 20) {
            this.level = 20;
            setExp(0);
            setMaxExp(0);
        }
        else {
            setExp(0);
            setMaxExp(expRef[this.level - 1]);
        }
    }

    public String getExp() {
        return Integer.toString(exp);
    }

    public int getExpInt() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public String getMaxExp() {
        return Integer.toString(maxExp);
    }

    public int getMaxExpInt() {
        return maxExp;
    }

    public void setMaxExp(int maxexp) {
        this.maxExp = maxexp;
    }

}
