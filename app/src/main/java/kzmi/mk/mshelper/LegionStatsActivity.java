package kzmi.mk.mshelper;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Iterator;

public class LegionStatsActivity extends AppCompatActivity {

    ArrayList<Character> warriors = new ArrayList<Character>();
    ArrayList<Character> magicians = new ArrayList<Character>();
    ArrayList<Character> archers = new ArrayList<Character>() ;
    ArrayList<Character> thieves = new ArrayList<Character>();
    ArrayList<Character> pirates = new ArrayList<Character>() ;
    ArrayList<Character> multiclass = new ArrayList<Character>();
    ArrayList<Character> missingJobs = new ArrayList<Character>();
    ArrayList<ArrayList<Character>> listOfLists = new ArrayList<ArrayList<Character>>();
    Resources resources;
    LegionStatsAdapter legionStatsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legion_stats);

        resources = getResources();
        String[] temp = resources.getStringArray(R.array.jobList_array);
        for (String x : temp)
            missingJobs.add(new Character("",x,0));

        ArrayList<Character> charList = (ArrayList<Character>) getIntent().getExtras().get("CHARLIST");
        for(Character x: charList) {
            if (x.getParentClass().equals("Warrior"))
                warriors.add(x);
            if (x.getParentClass().equals("Magician"))
                magicians.add(x);
            if (x.getParentClass().equals("Archer"))
                archers.add(x);
            if (x.getParentClass().equals("Thief"))
                thieves.add(x);
            if (x.getParentClass().equals("Pirate"))
                pirates.add(x);
            if (x.getParentClass().equals("Multiclass"))
                multiclass.add(x);
        }

        Iterator<Character> it = missingJobs.iterator();
        while (it.hasNext()) {
            Character x = it.next();
            for (Character y : charList) {
                if (x.getJob().equals(y.getJob()))
                    it.remove();
            }
        }
        listOfLists.add(warriors);
        listOfLists.add(magicians);
        listOfLists.add(archers);
        listOfLists.add(thieves);
        listOfLists.add(pirates);
        listOfLists.add(multiclass);
        listOfLists.add(missingJobs);

        RecyclerView recyclerView = findViewById(R.id.legionStats_mainView);
        legionStatsAdapter = new LegionStatsAdapter(this, listOfLists);
        recyclerView.setAdapter(legionStatsAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


    }
}
