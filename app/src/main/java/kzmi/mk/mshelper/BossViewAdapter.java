package kzmi.mk.mshelper;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

/*
    The BossViewAdapter class is used to create a custom viewing format with RecyclerView
 */
public class BossViewAdapter extends RecyclerView.Adapter<BossViewAdapter.ViewHolder> {

    private ArrayList<Boss> mBosses = new ArrayList<Boss>();
    private Context mContext;

    public BossViewAdapter(Context context, ArrayList<Boss> bosses) {
        mBosses = bosses;
        mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.boss_view, parent, false) ;
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        if (mBosses.get(i).getCount() == mBosses.get(i).getMaxCount()) { //Set the text to green if the current clears is equal to the maximum amount of clears.
            viewHolder.name.setTextColor(Color.GREEN);
            viewHolder.counter.setTextColor(Color.GREEN);
        }
        if (mBosses.get(i).getCount() != mBosses.get(i).getMaxCount()) { //Set the text to red if the current clears is unequal to the maximum amount of clears.
            viewHolder.name.setTextColor(Color.RED);
            viewHolder.counter.setTextColor(Color.RED);
        }
        viewHolder.name.setText(mBosses.get(i).getName());
        viewHolder.counter.setText(mBosses.get(i).getMergedString());
        viewHolder.adder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boss temp = mBosses.get(i);
                if(temp.getCount() != temp.getMaxCount()) {
                    temp.addCount();
                    mBosses.set(i, temp);
                    if (mBosses.get(i).getCount() == mBosses.get(i).getMaxCount()) { //Set the text to green if the current clears is equal to the maximum amount of clears.
                        viewHolder.name.setTextColor(Color.GREEN);
                        viewHolder.counter.setTextColor(Color.GREEN);
                    }
                    viewHolder.counter.setText(mBosses.get(i).getCount() + "/" + mBosses.get(i).getMaxCount());
                }
            }
        });
        viewHolder.reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //Resets the counter if a mistake was made or error has occurred.
                Boss temp = mBosses.get(i);
                temp.setCount(0);
                mBosses.set(i,temp);
                viewHolder.counter.setText(mBosses.get(i).getCount() + "/" + mBosses.get(i).getMaxCount());
                viewHolder.name.setTextColor(Color.RED);
                viewHolder.counter.setTextColor(Color.RED);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mBosses.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, counter;
        Button reset, adder;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.bossName);
            counter = itemView.findViewById(R.id.bossCounter);
            reset = itemView.findViewById(R.id.bossReset);
            adder = itemView.findViewById(R.id.adder_button);
        }
    }

    public ArrayList<Boss>getBossList() {
        return mBosses;
    }
}
