package kzmi.mk.mshelper;

import java.io.Serializable;
import java.util.ArrayList;

/*
    The Character class represents the statistical data of player's character. Currently this class
    tracks a character's name, class, level, and their daily and weekly bosses.
 */

public class Character implements Serializable{
    private String name, job, parentClass;
    private int level;
    private ArrayList<Boss> dailyBossList = new ArrayList<Boss>();
    private ArrayList<Boss> weeklyBossList = new ArrayList<Boss>();
    private ArcaneSymbol[] arcaneSymbolList = new ArcaneSymbol[6];
    private boolean fifthJobCheck = false;

    public Character(String name, String job, int level) {
        this.name = name;
        this.job = job;
        this.level = level;
        this.setParentClass(getJob());
        resetDailyBosses();
        resetWeeklyBosses();
        this.createArcaneSymbols();
    }

    public void resetDailyBosses() { //Currently used to reset the list of daily bosses
        dailyBossList.clear();
        if(this.level > 50)
            dailyBossList.add(new Boss("E/N Zakum", 1, 0));
        if(this.level > 170)
            dailyBossList.add(new Boss("Hilla", 1, 0));
        else if(this.level > 120)
            dailyBossList.add(new Boss("N Hilla", 1, 0));
        if(this.level > 125) {
            dailyBossList.add(new Boss("N Von Bon", 1, 0));
            dailyBossList.add(new Boss("N Pierre", 1, 0));
            dailyBossList.add(new Boss("N CQueen", 1, 0));
            dailyBossList.add(new Boss("N Vellum", 1, 0));
        }
        if(this.level > 135)
            dailyBossList.add(new Boss("Horntail", 1, 0));
        else if (this.level > 130)
            dailyBossList.add(new Boss("E Horntail", 1, 0));
        if(this.level > 155) {
            dailyBossList.add(new Boss("E/N Papulatus", 1, 0));
            dailyBossList.add(new Boss("E/N Magnus", 1, 0));
        }
        else if (this.level > 115) {
            dailyBossList.add(new Boss("E Papulatus", 1, 0));
            dailyBossList.add(new Boss("E Magnus ", 1, 0));
        }
        if(this.level > 170)
            dailyBossList.add(new Boss("Pink Bean", 1, 0));
        else if (this.level > 160)
            dailyBossList.add(new Boss("N Pink Bean", 1, 0));
        if(this.level > 125)
            dailyBossList.add(new Boss("Von Leon", 1, 0));
        else if (this.level > 120)
            dailyBossList.add(new Boss("E/N Von Leon", 1, 0));
        if(this.level > 140)
            dailyBossList.add(new Boss("Arkarium", 1, 0));
        if(this.level > 100)
            dailyBossList.add(new Boss("Ursus", 3, 0));
        if(this.level > 130)
            dailyBossList.add(new Boss("Gollux", 1, 0));
        if(this.level > 65)
            dailyBossList.add(new Boss("Balrog", 7, 0));
        if(this.level > 140)
            dailyBossList.add(new Boss("Julieta", 3, 0));
    }

    public void resetWeeklyBosses() { //Currently used to reset the list of weekly bosses.
        weeklyBossList.clear();
        if(this.level > 90)
            weeklyBossList.add(new Boss("C Zakum", 1, 0));
        if(this.level > 170)
            weeklyBossList.add(new Boss("Cygnus", 1, 0));
        else if(this.level > 140)
            weeklyBossList.add(new Boss("E Cygnus", 1, 0));
        if(this.level > 180) {
            weeklyBossList.add(new Boss("C Von Bon", 1, 0));
            weeklyBossList.add(new Boss("C Pierre", 1, 0));
            weeklyBossList.add(new Boss("C CQueen", 1, 0));
            weeklyBossList.add(new Boss("C Vellum", 1, 0));
        }
        if(this.level > 190) {
            weeklyBossList.add(new Boss("C Papulatus", 1, 0));
            weeklyBossList.add(new Boss("Lotus", 1, 0));
            weeklyBossList.add(new Boss("Damien", 1, 0));
        }
        if(this.level > 220)
            weeklyBossList.add(new Boss("Lucid", 1, 0));
        if(this.level > 235)
            weeklyBossList.add(new Boss("Will", 1, 0));
        if(this.level > 245)
            weeklyBossList.add(new Boss("Gloom", 1, 0));
        if(this.level > 250)
            weeklyBossList.add(new Boss("Verus Hilla", 1, 0));
        if(this.level > 255)
            weeklyBossList.add(new Boss("Darknell", 1, 0));
        if(this.level > 255)
            weeklyBossList.add(new Boss("Black Mage", 1, 0)); //Will be changed eventually. Black Mage is a monthly boss but the date of reset is currently unknown (First Thursday of the month?)
        if(this.level > 140)
            weeklyBossList.add(new Boss("Princess No", 10, 0));
    }

    public void createArcaneSymbols() {
        for(int i = 0; i < 6; i++)
        arcaneSymbolList[i] = new ArcaneSymbol();

    }

    //Sets the character name to a new name.
    public void setName(String name) {
        this.name = name;
    }

    //Sets the character level to a new value.
    public void setLevel(int level) {
        if(level > 275) {
            this.level = 275;
        }
        else if (level < 1) {
            this.level = 1;
        }
        else {
            this.level = level;
        }
    }

    //Automatically sets the parent class based on job.
    public void setParentClass(String job) {
        if(job.equals("Hero") == true || job.equals("Paladin") == true || job.equals("Hero") == true || job.equals("Dawn Warrior") == true || job.equals("Mihile") == true || job.equals("Demon Slayer") == true || job.equals("Demon Avenger") == true || job.equals("Blaster") == true || job.equals("Aran") == true || job.equals("Kaiser") == true || job.equals("Hayato") == true || job.equals("Zero") == true)
            this.parentClass = "Warrior";
        else if(job.equals("F/P Mage") == true || job.equals("I/L Mage") == true || job.equals("Bishop") == true || job.equals("Blaze Wizard") == true || job.equals("Battle Mage") == true || job.equals("Evan") == true || job.equals("Luminous") == true || job.equals("Kanna") == true || job.equals("Kinesis") == true || job.equals("Illium") == true || job.equals("Beast Tamer") == true)
            this.parentClass = "Magician";
        else if(job.equals("Bowmaster") == true || job.equals("Marksman") == true || job.equals("Wind Archer") == true || job.equals("Wild Hunter") == true || job.equals("Mercedes") == true)
            this.parentClass = "Archer";
        else if(job.equals("Night Lord") == true || job.equals("Shadower") == true || job.equals("Dual Blade") == true || job.equals("Night Walker") == true || job.equals("Phantom") == true || job.equals("Cadena") == true)
            this.parentClass = "Thief";
        else if(job.equals("Corsair") == true || job.equals("Buccaneer") == true || job.equals("Cannon Master") == true || job.equals("Jett") == true || job.equals("Thunder Breaker") == true || job.equals("Mechanic") == true || job.equals("Shade") == true || job.equals("Angelic Buster") == true || job.equals("Ark") == true)
            this.parentClass = "Pirate";
        else if(job.equals("Xenon"))
            this.parentClass = "Multiclass";
    }

    //Sets the character's job to a new job.
    public void setJob (String job) {
        this.job = job;
    }

    //Sets the character's list of daily bosses to a new list
    public void setDailyBossList(ArrayList<Boss> b) {
        this.dailyBossList = b;
    }

    //Sets the character's list of weekly bosses to a new list
    public void setWeeklyBossList(ArrayList<Boss> b) {
        this.weeklyBossList = b;
    }

    //Sets the character's list of arcane symbols to a new list
    public void setArcaneSymbolList(ArcaneSymbol[] arcaneSymbolList) {
        this.arcaneSymbolList = arcaneSymbolList;
    }

    //Returns if the the character has completed their 5th job advancement
    public void setFifthJobCheck(boolean fifthJobCheck) {
        this.fifthJobCheck = fifthJobCheck;
    }

    //Returns the character's name.
    public String getName(){
        return name;
    }

    //Returns the character's job.
    public String getJob() {
        return job;
    }

    //Returns the character's parent class.
    public String getParentClass() {
        return parentClass;
    }

    //Returns the character's level
    public int getLevel() {
        return level;
    }

    public int getLegionTier() {
        if(level < 60) {
            return 0;
        }
        else if (level < 100) {
            return 1;
        }
        else if (level < 140) {
            return 2;
        }
        else if (level < 200) {
            return 3;
        }
        else if (level < 250) {
            return 4;
        }
        else if (level >= 250) {
            return 5;
        }
        return 0;
    }

    //Returns the character's list of daily bosses
    public ArrayList<Boss> getDailyBossList() {
        return this.dailyBossList;
    }

    //Returns the character's list of weekly bosses
    public ArrayList<Boss> getWeeklyBossList() {
        return this.weeklyBossList;
    }

    //Returns the character's list of arcane symbols
    public ArcaneSymbol[] getArcaneSymbolList() {
        return arcaneSymbolList;
    }

    //Checks if the user has completed their 5th job advancement
    public boolean isFifthJobCheck() {
        return fifthJobCheck;
    }

}
