package kzmi.mk.mshelper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;


public class LegionStatsAdapter extends RecyclerView.Adapter<LegionStatsAdapter.ViewHolder> {

    private ArrayList<ArrayList<Character>> listOfCharLists;
    private Context mContext;
    private String[] parentClassNames = {"Warriors", "Magicians", "Bowmans", "Thieves", "Pirates", "Multiclass", "Missing Classes"};

    public LegionStatsAdapter(Context context, ArrayList<ArrayList<Character>> list){
        mContext = context;
        listOfCharLists = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.legionstats_view, parent, false) ;
        LegionStatsAdapter.ViewHolder holder = new LegionStatsAdapter.ViewHolder(view);
        return holder;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.classHeader.setText(parentClassNames[i]);
        RecyclerView recyclerView = viewHolder.recyclerView;
        LegionStatsSubAdapter subView = new LegionStatsSubAdapter(mContext, listOfCharLists.get(i));
        recyclerView.setAdapter(subView);
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false));

        viewHolder.recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
    }



    @Override
    public int getItemCount() {
        return 7;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView classHeader;
        RecyclerView recyclerView;

        public ViewHolder(View itemView) {
            super(itemView);
            classHeader = itemView.findViewById(R.id.legionStats_classHeader);
            recyclerView = itemView.findViewById(R.id.legionStats_infoList);

        }
    }
}

