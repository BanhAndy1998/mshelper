package kzmi.mk.mshelper;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import static android.content.Context.NOTIFICATION_SERVICE;

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        NotificationManager nm = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("MSHELPER", "Daily Notifications", NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("MSHelper notification for reset of daily routines");
            nm.createNotificationChannel(channel);
        }

        Notification b = new NotificationCompat.Builder(context, "MSHELPER")
                .setAutoCancel(true)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("MSHelper")
                .setContentText("Daily routines have reset!")
                .build();

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        mNotificationManager.notify(1, b);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        Gson gson = new Gson();
        String json = prefs.getString("charList", "");
        Type type = new TypeToken<ArrayList<Character>>(){}.getType();
        ArrayList<Character> tempList = gson.fromJson(json, type);

        ArrayList<Character> rTempList = new ArrayList<Character>();
        Calendar tempCal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        for(Character x : tempList) {
            x.resetDailyBosses();
            if(tempCal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) {
                x.resetWeeklyBosses();
            }
            rTempList.add(x);
        }

        json = gson.toJson(rTempList);
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString("charList", json);
        prefsEditor.commit();
    }
}
