package kzmi.mk.mshelper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import java.util.ArrayList;

/*
    The CharacterViewAdapter class is used to create a custom viewing format with RecyclerView.
 */

public class CharacterViewAdapter extends RecyclerView.Adapter<CharacterViewAdapter.ViewHolder> {
    private ArrayList<Character> mCharacters;
    private Context mContext;

    /*
        Constructor to be used with CharacterHubActivity
     */
    public CharacterViewAdapter(Context context, ArrayList<Character> characters) {
        mCharacters = characters;
        mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.characterselect_view, parent, false) ;
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        viewHolder.characterName.setText(mCharacters.get(i).getName());     //Please reference characterselect_view.xml for more info on lines 40-45
        viewHolder.className.setText(mCharacters.get(i).getParentClass());
        viewHolder.jobName.setText(mCharacters.get(i).getJob());
        viewHolder.jobName.setTextColor(Color.BLACK);
        viewHolder.level.setText(mCharacters.get(i).getLevel() + "");
        colorSet(viewHolder,i);
        viewHolder.characterName.setOnClickListener(new View.OnClickListener() {    //Branch for if character name is pressed
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(mContext, RoutinesActivity.class);
                Character transfer = mCharacters.get(i);
                startIntent.putExtra("ROUTINES", transfer);
                Activity origin = (Activity) mContext;
                origin.startActivityForResult(startIntent,1);
            }
        });
        viewHolder.characterEditButton.setOnClickListener(new View.OnClickListener() {  //Branch for if edit button is pressed
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(mContext, CharacterEditActivity.class);
                Character transfer = mCharacters.get(i);
                startIntent.putExtra("CHAREDIT",transfer);
                startIntent.putExtra("ARRAYPOSITION",i);
                Activity origin = (Activity) mContext;
                origin.startActivityForResult(startIntent,2);
            }
        });
    }

    /*
        Changes some of the text color based on the class of the character
     */
    public void colorSet(final ViewHolder viewHolder,final int i) {
        if(mCharacters.get(i).getParentClass().equals("Warrior")) {
            viewHolder.characterName.setTextColor(Color.RED);
            viewHolder.className.setTextColor(Color.RED);
            viewHolder.level.setTextColor(Color.RED);
        }
        if(mCharacters.get(i).getParentClass().equals("Magician")) {
            viewHolder.characterName.setTextColor(Color.BLUE);
            viewHolder.className.setTextColor(Color.BLUE);
            viewHolder.level.setTextColor(Color.BLUE);
        }
        if(mCharacters.get(i).getParentClass().equals("Bowman")) {
            viewHolder.characterName.setTextColor(Color.GREEN);
            viewHolder.className.setTextColor(Color.GREEN);
            viewHolder.level.setTextColor(Color.GREEN);
        }
        if(mCharacters.get(i).getParentClass().equals("Thief")) {
            viewHolder.characterName.setTextColor(Color.rgb(255,165,0));
            viewHolder.className.setTextColor(Color.rgb(255,165,0));
            viewHolder.level.setTextColor(Color.rgb(255,165,0));
        }
        if(mCharacters.get(i).getParentClass().equals("Pirate")) {
            viewHolder.characterName.setTextColor(Color.rgb(165,0,255));
            viewHolder.className.setTextColor(Color.rgb(165,0,255));
            viewHolder.level.setTextColor(Color.rgb(165,0,255));
        }
        if(mCharacters.get(i).getParentClass().equals("Multi")) {
            viewHolder.characterName.setTextColor(Color.rgb(190,190,0));
            viewHolder.className.setTextColor(Color.rgb(190,190,0));
            viewHolder.level.setTextColor(Color.rgb(190,190,0));
        }
    }

    @Override
    public int getItemCount() {
        return mCharacters.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView characterName, className, jobName, level;
        Button characterEditButton;

        public ViewHolder(View itemView) {
            super(itemView);
            characterName = itemView.findViewById(R.id.characterName);
            className = itemView.findViewById(R.id.className);
            jobName = itemView.findViewById(R.id.jobName);
            level = itemView.findViewById(R.id.characterLevel);
            characterEditButton = itemView.findViewById((R.id.characterEditButton));
        }
    }
}

