package kzmi.mk.mshelper;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

/*
    The CharacterEditActivity class allows for the user to edit a pre-existing or add a new character
    to their character list in CharacterHubActivity
 */
public class CharacterEditActivity extends AppCompatActivity {

    Character character;
    int i;              //Used to remember the character's position in the character list. Unused for new characters
    int levelCheck;     //Used to check if the level value was changed.
    EditText nameEdit;
    EditText levelEdit;
    Spinner classSelector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_edit);

        character = (Character) getIntent().getExtras().get("CHAREDIT");
        i = getIntent().getIntExtra("ARRAYPOSITION",-1);
        levelCheck = character.getLevel();

        nameEdit = findViewById(R.id.nameEdit);
        nameEdit.setText(character.getName());
        levelEdit = findViewById(R.id.levelEdit);
        levelEdit.setText(Integer.toString(character.getLevel()));
        classSelector = findViewById(R.id.classSelector);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.jobList_array, android.R.layout.simple_spinner_dropdown_item); //Spinner selection for class to avoid errors. Check classValues.xml for more info.
        classSelector.setAdapter(adapter);
        classSelector.setSelection(adapter.getPosition(character.getJob()));
    }

    /*
        Return to CharacterHubActivity with the edited or new character
     */
    @Override
    public void onBackPressed(){
        character.setName(nameEdit.getText().toString());
        character.setLevel(Integer.parseInt(levelEdit.getText().toString()));
        character.setJob(classSelector.getSelectedItem().toString());
        character.setParentClass(character.getJob());
        if(character.getLevel() != levelCheck) { //This currently resets ALL boss data, a new method will be implemented soon to retain current clears
            character.resetDailyBosses();
            character.resetWeeklyBosses();
        }

        Intent intent = new Intent();
        intent.putExtra("EDITEDCHAR", character);
        if(i >= 0)
            intent.putExtra("ARRAYPOSITION", i);
        setResult(RESULT_OK, intent);
        finish();
    }


}
