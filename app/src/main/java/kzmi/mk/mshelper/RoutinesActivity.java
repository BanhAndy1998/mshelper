package kzmi.mk.mshelper;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/*
    The RoutinesActivity class is where the user chooses which daily or weekly routines to track
    and edit for their selected character from CharacterHubActivity.
 */
public class RoutinesActivity extends AppCompatActivity {

    Character currentChar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routines);
        currentChar = (Character) getIntent().getExtras().get("ROUTINES"); //Please view activity_routines.xml for more information on the lines below.

        TextView dailyBossesRoutine = findViewById(R.id.routinesBosses);
        TextView weeklyBossesRoutine = findViewById(R.id.routinesWBosses);
        TextView arcaneRiverRoutine = findViewById(R.id.routinesArcane);

        dailyBossesRoutine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(v.getContext(), BossActivity.class);
                startIntent.putExtra("DAILYBOSS", currentChar.getDailyBossList());
                startActivityForResult(startIntent,1);
            }
        });

        weeklyBossesRoutine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(v.getContext(), BossActivity.class);
                startIntent.putExtra("WEEKLYBOSS", currentChar.getWeeklyBossList());
                startActivityForResult(startIntent,2);
            }
        });

        arcaneRiverRoutine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!currentChar.isFifthJobCheck()) {
                    Intent startIntent = new Intent(v.getContext(), FifthJobCheckActivity.class);
                    startIntent.putExtra("FIFTHJOB",currentChar);
                    startActivityForResult(startIntent,99);
                }

                else {
                    Intent startIntent = new Intent(v.getContext(), ArcaneRiverActivity.class);
                    startIntent.putExtra("ARCANESYMBOL", currentChar.getArcaneSymbolList());
                    startActivityForResult(startIntent,3);
                }
            }
        });


    }

    /*
        The following requestCodes correspond to the following application actions.
        1 - Returned from viewing daily boss routines.
        2 - Returned from viewing weekly boss routines.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                currentChar.setDailyBossList((ArrayList<Boss>) data.getExtras().get("NEWBOSS"));
            }
        }
        if (requestCode == 2) {
            if (resultCode == RESULT_OK) {
                currentChar.setWeeklyBossList((ArrayList<Boss>) data.getExtras().get("NEWBOSS"));
            }
        }
        if (requestCode == 3) {
            if (resultCode == RESULT_OK) {
                currentChar.setArcaneSymbolList((ArcaneSymbol[]) data.getExtras().get("ARCANESYMBOL"));
            }
        }
        if (requestCode == 99) {
            if (resultCode == RESULT_OK) {
                currentChar = (Character) data.getExtras().get("ARCANERIVERF");
                Intent startIntent = new Intent(this, ArcaneRiverActivity.class);
                startIntent.putExtra("ARCANESYMBOL", currentChar.getArcaneSymbolList());
                startActivityForResult(startIntent,3);
            }
        }
    }

    //Send the current character back to CharacterHubActivity for saving and return to character selection.
    @Override
    public void onBackPressed(){
        Intent intent = new Intent();
        intent.putExtra("UPDATEDCHAR", currentChar);
        setResult(RESULT_OK, intent);
        finish();
    }
}