package kzmi.mk.mshelper;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.logging.Level;

public class ArcaneRiverActivity extends AppCompatActivity {

    ArcaneSymbol[] arcaneSymbols = new ArcaneSymbol[6];
    ArcaneSymbolViewAdapter arcaneView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arcane_symbol);
        arcaneSymbols = (ArcaneSymbol[]) getIntent().getExtras().get("ARCANESYMBOL");

        RecyclerView recyclerView = findViewById(R.id.arcaneSymbolView);
        arcaneView = new ArcaneSymbolViewAdapter(this, arcaneSymbols);
        recyclerView.setAdapter(arcaneView);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent();
        intent.putExtra("ARCANESYMBOL", arcaneSymbols);
        setResult(RESULT_OK, intent);
        finish();
    }
}
