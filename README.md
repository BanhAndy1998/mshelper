This is a tracking app made for Android Devices for the popular MMORPG Maplestory. Track all your daily and weekly routines for all of your characters!
Updates to this app will be very slow as I am currently attending college classes. 

As of April 24th, development on this app will be put on hiatus. I have stopped playing Maplestory for
several weeks and as such I have no desire to work on the app. 

List of Features
--------------------------------------------
- Daily Bosses (DONE!)
- Weekly Bosses (DONE!)
- Monthly Boss? (TO BE IMPLEMENTED)
- Arcane Symbols (DONE!)
- Dark World Tree & Scrapyard Dailies (TO BE IMPLEMENTED)
- Commerci Voyages & PQ (TO BE IMPLEMENTED)
- Kritias Dailies (TO BE IMPLEMENTED)
- Monster Park Dailies (TO BE IMPLEMENTED)
- Legion Statistics (DONE!)
- Legion Board Simulator (TO BE IMPLEMENTED)
- Notifications on Daily Resets (DONE!)
- Automatic Daily Reset (DONE!)

If you'd like a certain feature added then please email me at MK_KZMI@tuta.io